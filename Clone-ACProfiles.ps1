﻿# Script for cloning Virindi Tank and Utility Belt profiles from one server to another

# Path to Decal Plugin Data folder
$DecalPluginDataFolder = "$ENV:UserProfile\Documents\Decal Plugins"

# Path to Virindi Tank folder
$VirindiTankFolder = "C:\Games\VirindiPlugins\VirindiTank"

# Original server name
$OriginalServerName = "Sundering"

# New server name for future use
$NewServerName = "Test Sundering"

# Whether to copy Utility Belt Profiles ($True|$False)
$CopyUBProfiles = $False

# Check if Virindi Tank folder exists
if (Test-Path -Path $VirindiTankFolder) {
    # Search for files matching pattern: CharacterName_OriginalServerName*.usd
    $filePattern = "*_$($OriginalServerName)*.usd"
    $files = Get-ChildItem -Path $VirindiTankFolder -Filter $filePattern

    # Create a hash table for DisplayName to FullPath mapping
    $fileMap = @{}

    # Check if any files were found
    if ($files.Count -gt 0) {
        # Process each file and add to hash table
        $files | ForEach-Object {
            $displayName = $_.Name.Replace('.usd', '').TrimStart('-')
            $fileMap[$displayName] = $_.FullName
        }

        # Sort DisplayNames and display in a GridView
        $sortedDisplayNames = $fileMap.Keys | Sort-Object
        $selectedDisplayNames = $sortedDisplayNames | Out-GridView -Title "Select Files" -OutputMode Multiple

        # Output the selected item names and retrieve FullPath from hash table
        if ($selectedDisplayNames) {
            Write-Host "Selected Items and Accompanying Files:"
            foreach ($displayName in $selectedDisplayNames) {
                $fullPath = $fileMap[$displayName]
                Write-Host "Name: $displayName"
                Write-Host "Full Path: $fullPath"

                # Finding and copying accompanying .nav, .met, and .ast files
                $baseName = [System.IO.Path]::GetFileNameWithoutExtension($fullPath)
                $directory = [System.IO.Path]::GetDirectoryName($fullPath)
                $extensions = @('.usd', '.nav', '.met', '.ast')

                foreach ($ext in $extensions) {
                    $originalFilePath = Join-Path $directory "$baseName$ext"
                    if (Test-Path $originalFilePath) {
                        # Create new file name with new server name
                        $newFileName = $baseName -replace $OriginalServerName, $NewServerName
                        $newFilePath = Join-Path $directory "$newFileName$ext"

                        # Copy files
                        Copy-Item -Path $originalFilePath -Destination $newFilePath -Verbose -Force
                    }
                }
            }
        } else {
            Write-Host "No items selected."
        }
    } else {
        Write-Host "No files found matching the pattern in $VirindiTankFolder"
    }
} else {
    Write-Host "Virindi Tank Folder does not exist: $VirindiTankFolder"
}

# Clone server-specific utility belt profiles (If enabled)
if ($CopyUBProfiles -eq $True){
    $SourceUBFolder="$DecalPluginDataFolder\UtilityBelt\$OriginalServerName"
    $TargetUBFolder="$DecalPluginDataFolder\UtilityBelt\$NewServerName"
    # Create container folder if it doesn't already exist
    if (not (Test-Path $TargetUBFolder)){
        new-item -Path $TargetUBFolder -ItemType Directory
    }
    Write-Output "Cloning Utility Belt Profiles" |Out-Host
    Copy-Item -Path $SourceUBFolder -Destination $TargetUBFolder -Recurse -Force -Verbose
}